EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_shieldsNCL
LIBS:switches
LIBS:Visualizer-cache
EELAYER 25 0
EELAYER END
$Descr USLedger 17000 11000
encoding utf-8
Sheet 1 3
Title "Visualizer"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5250 2950 5250 4200
$Comp
L GND #PWR?
U 1 1 58F38AA4
P 4500 5500
F 0 "#PWR?" H 4500 5250 50  0001 C CNN
F 1 "GND" H 4500 5350 50  0000 C CNN
F 2 "" H 4500 5500 50  0001 C CNN
F 3 "" H 4500 5500 50  0001 C CNN
	1    4500 5500
	1    0    0    -1  
$EndComp
Text Label 4800 4200 0    60   ~ 0
Vs
Wire Wire Line
	5250 4200 4650 4200
$Comp
L JACK_TRS_6PINS J?
U 1 1 5953C81F
P 3350 2600
F 0 "J?" H 3350 3000 50  0000 C CNN
F 1 "JACK_TRS_6PINS" H 3300 2300 50  0000 C CNN
F 2 "" H 3450 2450 50  0001 C CNN
F 3 "" H 3450 2450 50  0001 C CNN
	1    3350 2600
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5953CAFB
P 5750 2800
F 0 "R?" V 5830 2800 50  0000 C CNN
F 1 "22k" V 5750 2800 50  0000 C CNN
F 2 "" V 5680 2800 50  0001 C CNN
F 3 "" H 5750 2800 50  0001 C CNN
	1    5750 2800
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5953CB7C
P 5750 2600
F 0 "R?" V 5830 2600 50  0000 C CNN
F 1 "22k" V 5750 2600 50  0000 C CNN
F 2 "" V 5680 2600 50  0001 C CNN
F 3 "" H 5750 2600 50  0001 C CNN
	1    5750 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	3750 2600 5600 2600
Wire Wire Line
	3750 2800 5600 2800
$Comp
L GND #PWR?
U 1 1 5953CDD2
P 5400 2400
F 0 "#PWR?" H 5400 2150 50  0001 C CNN
F 1 "GND" H 5400 2250 50  0000 C CNN
F 2 "" H 5400 2400 50  0001 C CNN
F 3 "" H 5400 2400 50  0001 C CNN
	1    5400 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 2400 5400 2400
Text Label 4350 2800 0    60   ~ 0
RIGHT_IN
Text Label 4350 2600 0    60   ~ 0
LEFT_IN
Wire Wire Line
	5900 2800 7500 2800
Wire Wire Line
	6000 2600 5900 2600
Connection ~ 6000 2800
$Sheet
S 7500 2700 800  650 
U 59540D1F
F0 "Equalizer" 60
F1 "Equalizer.sch" 60
F2 "OUT" O R 8300 2800 60 
F3 "IN" I L 7500 2800 60 
F4 "VS" I L 7500 2950 60 
F5 "STROBE" I L 7500 3100 60 
F6 "RESET" I L 7500 3250 60 
$EndSheet
Wire Wire Line
	6000 2600 6000 2800
Text Label 6200 2800 0    60   ~ 0
SIN
Wire Wire Line
	5250 2950 7500 2950
$Sheet
S 2900 3850 1000 200 
U 5954F8FF
F0 "PowerSupply" 60
F1 "PowerSupply.sch" 60
F2 "VI" I L 2900 3950 60 
F3 "VO" O R 3900 3950 60 
$EndSheet
Wire Wire Line
	2500 3950 2900 3950
Wire Wire Line
	3900 3950 4650 3950
Connection ~ 4650 4200
$Comp
L ARDUINO_SHIELD SHIELD?
U 1 1 5956445D
P 5950 5500
F 0 "SHIELD?" H 5600 6450 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 6000 4550 60  0000 C CNN
F 2 "" H 5950 5500 60  0001 C CNN
F 3 "" H 5950 5500 60  0001 C CNN
	1    5950 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3950 4650 5300
Wire Wire Line
	4650 5300 5000 5300
Wire Wire Line
	4500 5500 4500 5400
Wire Wire Line
	3650 5400 5000 5400
$Comp
L SW_Push_Dual SW?
U 1 1 59567744
P 4050 4800
F 0 "SW?" H 4100 4900 50  0000 L CNN
F 1 "SW_Push_Dual" H 4050 4530 50  0000 C CNN
F 2 "" H 4050 5000 50  0001 C CNN
F 3 "" H 4050 5000 50  0001 C CNN
	1    4050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5100 4750 5100
Wire Wire Line
	4750 5100 4750 4800
Wire Wire Line
	4750 4800 4250 4800
Wire Wire Line
	3850 4800 3650 4800
Wire Wire Line
	3650 4800 3650 5400
Connection ~ 4500 5400
Wire Wire Line
	4650 6600 8400 6600
Wire Wire Line
	4650 6600 4650 5800
Wire Wire Line
	4650 5800 5000 5800
Wire Wire Line
	7200 5700 6900 5700
Wire Wire Line
	7200 3100 7200 5700
Wire Wire Line
	7350 3250 7350 6100
Wire Wire Line
	7350 6100 6900 6100
$Comp
L JACK_TRS_6PINS J?
U 1 1 5956E857
P 3350 1700
F 0 "J?" H 3350 2100 50  0000 C CNN
F 1 "JACK_TRS_6PINS" H 3300 1400 50  0000 C CNN
F 2 "" H 3450 1550 50  0001 C CNN
F 3 "" H 3450 1550 50  0001 C CNN
	1    3350 1700
	1    0    0    -1  
$EndComp
$Comp
L BARREL_JACK J?
U 1 1 5956F6A6
P 2200 4050
F 0 "J?" H 2200 4245 50  0000 C CNN
F 1 "BARREL_JACK" H 2200 3895 50  0000 C CNN
F 2 "" H 2200 4050 50  0001 C CNN
F 3 "" H 2200 4050 50  0001 C CNN
	1    2200 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5956F988
P 2600 4200
F 0 "#PWR?" H 2600 3950 50  0001 C CNN
F 1 "GND" H 2600 4050 50  0000 C CNN
F 2 "" H 2600 4200 50  0001 C CNN
F 3 "" H 2600 4200 50  0001 C CNN
	1    2600 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4150 2600 4150
Wire Wire Line
	2600 4050 2600 4200
Wire Wire Line
	2500 4050 2600 4050
Connection ~ 2600 4150
Wire Wire Line
	3750 1900 3900 1900
Wire Wire Line
	3900 1900 3900 2800
Connection ~ 3900 2800
Wire Wire Line
	3750 1700 4100 1700
Wire Wire Line
	4100 1700 4100 2600
Connection ~ 4100 2600
Wire Wire Line
	3750 1500 4300 1500
Wire Wire Line
	4300 1500 4300 2400
Connection ~ 4300 2400
Wire Wire Line
	7200 3100 7500 3100
Wire Wire Line
	7350 3250 7500 3250
Wire Wire Line
	8300 2800 8400 2800
Wire Wire Line
	8400 2800 8400 6600
Wire Wire Line
	5000 5500 4900 5500
Wire Wire Line
	4900 5500 4900 5400
Connection ~ 4900 5400
$Comp
L GND #PWR?
U 1 1 5957F88A
P 6900 4800
F 0 "#PWR?" H 6900 4550 50  0001 C CNN
F 1 "GND" H 6900 4650 50  0000 C CNN
F 2 "" H 6900 4800 50  0001 C CNN
F 3 "" H 6900 4800 50  0001 C CNN
	1    6900 4800
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
