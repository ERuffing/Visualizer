/**
 * \file msgeq7.h
 * \author Ethan Ruffing
 * \since 2017-07-11
 */ 

#include <stdint.h>

#ifndef MSGEQ7_H_
#define MSGEQ7_H_

/**
 * A structure defining an MSGEQ7 chip.
 */
struct msgeq7 {
	/** The port to which the strobe pin is attached. */
	volatile uint8_t *strobeport;
	/** The data direction register to which the strobe pin is attached. */
	volatile uint8_t *strobeddr;
	/** The pin number within the port to which the strobe pin is attached. */
	uint8_t strobepin;
	/** The port to which the reset pin is attached. */
	volatile uint8_t *resetport;
	/** The data direction register to which the reset pin is attached. */
	volatile uint8_t *resetddr;
	/** The pin number within the port to which the reset pin is attached. */
	uint8_t resetpin;
	/** The ADC pin number to which the output pin is attached. */
	uint8_t outputadc;
};
typedef struct msgeq7* Msgeq7;

/**
 * Initialize the msgeq7. Note that the returned structure has been malloc'd
 * and must be free'd when done.
 * 
 * \param strobeport[in]    The port to which the strobe pin is attached.
 * \param strobeddr[in]     The data direction register to which the strobe pin
 *                          is attached.
 * \param strobepin         The pin number within the port to which the strobe
 *                          pin is attached.
 * \param resetport[in]     The port to which the reset pin is attached.
 * \param resetddr[in]      The data direction register to which the reset pin is attached.
 * \param resetpin          The pin number within the port to which the reset
 *                          pin is attached.
 * \param outputadc         The ADC pin number to which the output pin is
 *                          attached.
 * \return                  The initialized structure for the equalizer.
 */
Msgeq7 msgeq7Init(volatile uint8_t *strobeport, volatile uint8_t *strobeddr,
                  uint8_t strobepin, volatile uint8_t *resetport,
				  volatile uint8_t *resetddr, uint8_t resetpin,
                  uint8_t outputadc);

/**
 * Get the spectrum from the specified msgeq7. Note that the returned structure
 * has been malloc'd and must be free'd when done.
 * 
 * \param[in] eq The equalizer to query.
 * \return The spectrum read from the equalizer.
 */
uint16_t* getSpectrum(const Msgeq7 eq);

/**
 * Update the spectrum from the specified msgeq7.
 * 
 * \param[in] eq The equalizer to query.
 * \param[out] sp The spectrum structure to place the results in.
 */
void updateSpectrum(const Msgeq7 eq, uint16_t* sp);

#endif /* MSGEQ7_H_ */