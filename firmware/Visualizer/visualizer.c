/**
 * \file visualizer.c
 * \author Ethan Ruffing
 *
 * Main file for visualizer project.
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include "msgeq7.h"

// Include UART and set up necessary macros
#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1
#include "uart.h"
#include <stdio.h>

#include <util/delay.h>

#define SET_LED1_BRIGHTNESS(x) (OCR2A=x)
#define SET_LED2_BRIGHTNESS(x) (OCR1BL=x)
#define SET_LED3_BRIGHTNESS(x) (OCR1AL=x)

/**
 * Initialize timers for PWM as appropriate for controlling LEDs.
 */
void timerInit(void){
	//timer2 in fast PWM mode and inverting output on OCR2
	TCCR2A |= (1<<COM2A1)|(1<<COM2A0)|(1<<WGM21)|(1<<WGM20);
	TCCR2B |= (1<<CS20);
	
	//timer1 in fast PWM mode and inverting output on OCR1A and OCR1B
	TCCR1A |= ((1<<COM1A0)|(1<<COM1A1)|(1<<WGM10)|(1<<COM1B0)|(1<<COM1B1));
	TCCR1B |= (1<<WGM12)|(1<<CS10);
}

int main(void) {
    USART_Init(MYUBRR);
	timerInit();
	stdout = &uart_output;
	stdin = &uart_input;
	
	// Initialize I/O
	DDRB=0XFF; //PORTB as output
	PORTB=0XFF; //PORTB as current source
	
	// Initialize MSGEQ7
	Msgeq7 eq = msgeq7Init(&PORTD, &DDRD, 2, &PORTD, &DDRD, 6, 0);
	
	// Initialize a structure for the spectrum results
	uint16_t* sp = malloc(sizeof(uint16_t) * 7);
	while(1) {
		updateSpectrum(eq, sp);
		SET_LED1_BRIGHTNESS(sp[0]/4);
		SET_LED2_BRIGHTNESS(sp[3]/4);
		SET_LED3_BRIGHTNESS(sp[6]/4);
	}
	
	free(sp);
	free(eq);
}

